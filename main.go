package main

import (
	"bufio"
	"bytes"
	"database/sql"
	"flag"
	"fmt"
	"os"
	"reflect"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

var (
	file   string
	mode   string
	cache  string
	db     *sql.DB
	reader = bufio.NewReader(os.Stdin)
)

func main() {
	flag.StringVar(&file, "f", "test.db", "the file to open")
	flag.StringVar(&mode, "m", "rwc", "the mode for which to open the file")
	flag.StringVar(&cache, "c", "shared", "the mode for which to set the cache")
	flag.Parse()
	var err error
	if db, err = sql.Open("sqlite3", fmt.Sprintf("file:%s?mode=%s&cache=%s", file, mode, cache)); err != nil {
		panic(err)
	}
	for !print(exec(read())) {
	}
}

func read() string {
	fmt.Print("sqlite> ")
	input, _ := reader.ReadString('\n')
	return strings.TrimRight(input, " \r\n")
}

func exec(input string) (string, bool) {
	if "exit" == input {
		return "", true
	}
	if rows, err := db.Query(input); err != nil {
		return err.Error(), false
	} else {
		return formatRows(rows), false
	}
}

func print(output string, exit bool) bool {
	fmt.Println(output)
	return exit
}

func formatRows(rows *sql.Rows) string {
	defer rows.Close()
	var buffer bytes.Buffer
	template := "%15v"
	if names, err := rows.Columns(); err != nil {
		return err.Error()
	} else if len(names) > 0 {
		for _, name := range names {
			buffer.WriteString("|")
			buffer.WriteString(fmt.Sprintf(template, name))
		}
		buffer.WriteString("|\n")
	}
	for rows.Next() {
		scanners := []interface{}{}
		if types, err := rows.ColumnTypes(); err != nil {
			return err.Error()
		} else {
			for _, t := range types {
				if t.ScanType() != nil {
					scan := reflect.New(t.ScanType()).Interface()
					scanners = append(scanners, scan)
				} else {
					var i interface{}
					scanners = append(scanners, &i)
				}
			}
		}
		if err := rows.Scan(scanners...); err != nil {
			return err.Error()
		}
		for _, s := range scanners {
			buffer.WriteString("|")
			v := reflect.ValueOf(s).Elem().Interface()
			buffer.WriteString(fmt.Sprintf(template, v))
		}
		buffer.WriteString("|\n")
	}
	return buffer.String()
}
